FintechBank - Consola de administraci�n
====================

Esta aplicaci�n web permitir� a los administradores, configurar los par�metros necesarios para la operaci�n de las aplicaciones IOS y Android del proyecto **FintechBank**

- - -

## FintechBank
Aplicaci�n m�vil que incluir� adem�s de servicios bancarios; consulta de saldos, transferencias entre cuentas propias y transferencias interbancarias, integraci�n con soft token, pagos de servicios (CFE, Telmex, etc), remesas y cr�ditos.

- - -

## Prerrequisitos

Para preparar tu ambiente de desarrollo se requiere tener instalado:

* PHP 5
* Laravel
* Xcode
* Firebase
* Auth0

- - -

## Configuraci�n de ambiente de desarrollo

1. Instalar Apache
2. Instalar PHP5
3. Instalar Mysql
4. Descargar con Composer Laravel
5. Clonar el repositorio git@bitbucket.org:diegovis/workshop-git.git 
6. Modificar el archivo de configuraci�n /proyecto/config.php agregar la cadena de conexi�n

- - -

## Despliegue

1. Conectarse por SSH al servidor Digital Ocean
2. Navegar al directorio donde se encuentra publicado el proyecto
3. Realizar pull del proyecto
4. Conectarse a la BD de este ambiente y ejecutar el script que se encuentra en /App_Data/last-updates.sql
5. Utilizando un browser confirmar que el despliegue haya sido exitoso

- - -

## Versionamento

Utilizamos [SemVer](https://semver.org/ ) para el versionamiento del proyecto 

- - -

## Autor

Inflexion 
http://inflexionsoftware.com 
![Inflexion](http://inflexionsoftware.com/images/logo-inflexion-1.png)

